package antlr;

import lombok.Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@Data
public class Test extends ArrayList<Integer> {
    private List<Long> timestamps = new ArrayList<>();

    public Integer findGreater() {
        timestamps.add(System.currentTimeMillis());
        return Collections.max(this);
    }

    public Integer findLeast() {
        timestamps.add(System.currentTimeMillis());
        return Collections.min(this);
    }

    public Integer rotate() {
        timestamps.add(System.currentTimeMillis());
        Collections.rotate(this, this.size());

        return 1;
    }

    public void generateCollections(Integer count, Integer min, Integer max) {
        for (int i = 0; i < count; i++) {
            this.add(getRandomNumberUsingNextInt(min, max));
        }
    }

    public int getRandomNumberUsingNextInt(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

    public void clearStamps() {
        timestamps = new ArrayList<>();
    }
}

