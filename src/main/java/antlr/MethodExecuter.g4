// Define a grammar called Hello
grammar MethodExecuter;

@header {
import  java.util.*;
import org.apache.commons.lang3.reflect.MethodUtils;
import java.util.HashMap;
}

exec returns [int n, Map<String, Object> objects]
           : expr*
           ;
expr: OBJECT DOT METHOD;

OBJECT : [a-zA-Z_]+ ;             // match lower-case identifiers
DOT: [.];
METHOD: [a-zA-Z_]+[(][)] ;
WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines