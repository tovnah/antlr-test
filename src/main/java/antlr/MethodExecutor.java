package antlr;

import lombok.SneakyThrows;
import org.apache.commons.lang3.reflect.MethodUtils;


public class MethodExecutor {
    @SneakyThrows
    public static Integer execute(Object o, String meth) {
        String editedMeth = meth.replace("()", "");

        Object o1 = MethodUtils.invokeMethod(o, editedMeth);

        return (Integer) o1;
    }
}
