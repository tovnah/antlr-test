package antlr.gen;

import antlr.MethodExecutor;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;

public class MethodExecuterVisitorImpl implements MethodExecuterVisitor<Integer> {
    @Override
    public Integer visitExec(MethodExecuterParser.ExecContext ctx) {
        for (MethodExecuterParser.ExprContext exprContext : ctx.expr()) {
            visitExpr(exprContext);
        }

        return null;
    }

    @Override
    public Integer visitExpr(MethodExecuterParser.ExprContext ctx) {
        MethodExecuterParser.ExecContext parent = (MethodExecuterParser.ExecContext) ctx.parent;
        String method = ctx.METHOD().getText();
        String obj = ctx.OBJECT().getText();

        Object o = parent.objects.get(obj);

        return MethodExecutor.execute(o, method);
    }

    @Override
    public Integer visit(ParseTree parseTree) {
        return null;
    }

    @Override
    public Integer visitChildren(RuleNode ruleNode) {
        return null;
    }

    @Override
    public Integer visitTerminal(TerminalNode terminalNode) {
        return null;
    }

    @Override
    public Integer visitErrorNode(ErrorNode errorNode) {
        return null;
    }
}
