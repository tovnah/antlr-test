// Generated from /home/abolotov/projects/antlr-test/src/main/java/antlr/MethodExecuter.g4 by ANTLR 4.9.1
package antlr.gen;

import  java.util.*;
import org.apache.commons.lang3.reflect.MethodUtils;
import java.util.HashMap;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MethodExecuterParser}.
 */
public interface MethodExecuterListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MethodExecuterParser#exec}.
	 * @param ctx the parse tree
	 */
	void enterExec(MethodExecuterParser.ExecContext ctx);
	/**
	 * Exit a parse tree produced by {@link MethodExecuterParser#exec}.
	 * @param ctx the parse tree
	 */
	void exitExec(MethodExecuterParser.ExecContext ctx);
	/**
	 * Enter a parse tree produced by {@link MethodExecuterParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(MethodExecuterParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link MethodExecuterParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(MethodExecuterParser.ExprContext ctx);
}