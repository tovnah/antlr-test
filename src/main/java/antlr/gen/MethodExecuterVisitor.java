// Generated from /home/abolotov/projects/antlr-test/src/main/java/antlr/MethodExecuter.g4 by ANTLR 4.9.1
package antlr.gen;

import  java.util.*;
import org.apache.commons.lang3.reflect.MethodUtils;
import java.util.HashMap;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link MethodExecuterParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface MethodExecuterVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link MethodExecuterParser#exec}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExec(MethodExecuterParser.ExecContext ctx);
	/**
	 * Visit a parse tree produced by {@link MethodExecuterParser#expr}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpr(MethodExecuterParser.ExprContext ctx);
}