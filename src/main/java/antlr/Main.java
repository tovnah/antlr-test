package antlr;

import antlr.gen.MethodExecuterLexer;
import antlr.gen.MethodExecuterParser;
import antlr.gen.MethodExecuterVisitor;
import antlr.gen.MethodExecuterVisitorImpl;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Main {
    public static void main(String[] args) throws Exception {
        Test test = new Test();
        test.generateCollections(1000, 0, Integer.MAX_VALUE);

//        double java = calcAverageForJava(test);

//        double antlrWithOneInitialization = calcAverageForAntlrWithOneInitialization(test);
//        double averageForAntlrWithEveryTimeInitialization = calcAverageForAntlrWithEveryTimeInitialization(test);
//        double antlrLexer = calcAntlrLexer(test);
        double averageForAntlrWithOneInitializationFewLexers = calcAverageForAntlrWithOneInitializationFewLexers(test);
        double averageForJavaFew = calcAverageForJavaFew(test);
//        System.out.println("Average java: " + java);
//        System.out.println("Average antlr one initialization: " + antlrWithOneInitialization);
//        System.out.println("Average antlr everytime initialization: " + averageForAntlrWithEveryTimeInitialization);
//        System.out.println("Average antlr 100 times command: " + antlrLexer);
        System.out.println("Average java 3 methods: " + averageForJavaFew);
        System.out.println("Average antlr one initialization 3 lexers: " + averageForAntlrWithOneInitializationFewLexers);

    }

    private static double calcAntlrLexer(Test test) {
        List<Long> times = new ArrayList<>();
        StringBuilder cmd = new StringBuilder();
        for (int i = 0; i < 100; i++) {
            cmd.append("test.findGreater()\n");
        }
        long start = System.currentTimeMillis();
        MethodExecuterLexer lexer1 = new MethodExecuterLexer(CharStreams.fromString(cmd.toString()));
        CommonTokenStream tokens = new CommonTokenStream(lexer1);
        MethodExecuterParser parser1 = new MethodExecuterParser(tokens);
        MethodExecuterParser.ExecContext exec = parser1.exec();


        exec.objects = new HashMap<String, Object>() {{
            put("test", test);
        }};
        MethodExecuterVisitor<Integer> visitor = new MethodExecuterVisitorImpl();
        Integer greater = visitor.visitExec(exec);
        System.out.println(greater);
        times.add(System.currentTimeMillis() - start);


        return calcAverage(times);
    }

    private static double calcAverageForJava(Test test) {
        List<Long> times = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            long start = System.currentTimeMillis();
            Integer greater = test.findGreater();
            times.add(System.currentTimeMillis() - start);
            System.out.println(greater);
        }
        return calcAverage(times);
    }


    private static double calcAverageForJavaFew(Test test) {
        List<Long> times = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            long start = System.currentTimeMillis();
            Integer greater = test.findGreater();
            Integer least = test.findLeast();
            Integer rotate = test.rotate();
            times.add(System.currentTimeMillis() - start);
            System.out.println(greater);
            System.out.println(least);
            System.out.println(rotate);
        }

        return calcAverage(times);
    }

    private static double calcAverageForAntlrWithOneInitialization(Test test) {
        List<Long> times = new ArrayList<>();
        MethodExecuterLexer lexer1 = new MethodExecuterLexer(CharStreams.fromString("test.findGreater()"));
        CommonTokenStream tokens = new CommonTokenStream(lexer1);
        MethodExecuterParser parser1 = new MethodExecuterParser(tokens);
        MethodExecuterParser.ExecContext exec = parser1.exec();

        for (int i = 0; i < 1000; i++) {
            long start = System.currentTimeMillis();

            exec.objects = new HashMap<String, Object>() {{
                put("test", test);
            }};
            MethodExecuterVisitor<Integer> visitor = new MethodExecuterVisitorImpl();
            Integer greater = visitor.visitExec(exec);
            System.out.println(greater);
            times.add(System.currentTimeMillis() - start);
        }

        return calcAverage(times);
    }

    private static double calcAverageForAntlrWithOneInitializationFewLexers(Test test) {
        List<Long> times = new ArrayList<>();
        MethodExecuterLexer lexer1 = new MethodExecuterLexer(CharStreams.fromString("test.findGreater()"));
        MethodExecuterLexer lexer2 = new MethodExecuterLexer(CharStreams.fromString("test.findLeast()"));
        MethodExecuterLexer lexer3 = new MethodExecuterLexer(CharStreams.fromString("test.rotate()"));
        CommonTokenStream tokens = new CommonTokenStream(lexer1);
        CommonTokenStream tokens2 = new CommonTokenStream(lexer2);
        CommonTokenStream tokens3 = new CommonTokenStream(lexer3);
        MethodExecuterParser parser1 = new MethodExecuterParser(tokens);
        MethodExecuterParser parser2 = new MethodExecuterParser(tokens2);
        MethodExecuterParser parser3 = new MethodExecuterParser(tokens3);
        MethodExecuterParser.ExecContext exec = parser1.exec();
        MethodExecuterParser.ExecContext exec2 = parser2.exec();
        MethodExecuterParser.ExecContext exec3 = parser3.exec();

        for (int i = 0; i < 1000; i++) {
            long start = System.currentTimeMillis();

            exec.objects = new HashMap<String, Object>() {{
                put("test", test);
            }};

            exec2.objects = new HashMap<String, Object>() {{
                put("test", test);
            }};

            exec3.objects = new HashMap<String, Object>() {{
                put("test", test);
            }};
            MethodExecuterVisitor<Integer> visitor = new MethodExecuterVisitorImpl();
            Integer greater = visitor.visitExec(exec);
            Integer least = visitor.visitExec(exec2);
            Integer rotate = visitor.visitExec(exec3);
            System.out.println("Greatest: " + greater);
            System.out.println("Least: " + least);
            System.out.println("rotate: " + rotate);

            times.add(System.currentTimeMillis() - start);
        }

        return calcAverage(times);
    }

    private static double calcAverageForAntlrWithEveryTimeInitialization(Test test) {
        List<Long> times = new ArrayList<>();

        for (int i = 0; i < 1000; i++) {
            long start = System.currentTimeMillis();
            MethodExecuterLexer lexer1 = new MethodExecuterLexer(CharStreams.fromString("test.findGreater()"));
            CommonTokenStream tokens = new CommonTokenStream(lexer1);
            MethodExecuterParser parser1 = new MethodExecuterParser(tokens);
            MethodExecuterParser.ExecContext exec = parser1.exec();

            exec.objects = new HashMap<String, Object>() {{
                put("test", test);
            }};
            MethodExecuterVisitor<Integer> visitor = new MethodExecuterVisitorImpl();
            Integer greater = visitor.visitExec(exec);
            System.out.println(greater);
            times.add(System.currentTimeMillis() - start);
        }

        return calcAverage(times);
    }


    public static double calcAverage(List<Long> times) {
        Long sum = 0L;
        for (Long time : times) {
            sum += time;
        }
        return (double) sum / (double) times.size();
    }
}
