package antlr;

public interface Processor {
    @OurDslAnnotation(query = "test.findGreater")
    Integer getGreater(Test test);
}
