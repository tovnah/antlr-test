package antlr;

import antlr.gen.MethodExecuterLexer;
import antlr.gen.MethodExecuterParser;
import antlr.gen.MethodExecuterVisitor;
import antlr.gen.MethodExecuterVisitorImpl;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class MainTest {
    @Test
    @SneakyThrows
    void calcAverageForAntlrWithOneInitialization() {
        List<Long> times = new ArrayList<>();
        MethodExecuterLexer lexer1 = new MethodExecuterLexer(CharStreams.fromString("test.findGreater()"));
        CommonTokenStream tokens = new CommonTokenStream(lexer1);
        MethodExecuterParser parser1 = new MethodExecuterParser(tokens);
        MethodExecuterParser.ExecContext exec = parser1.exec();

        for (int i = 0; i < 1000; i++) {
            long start = System.currentTimeMillis();

            exec.objects = new HashMap<String, Object>() {{
                put("test", test);
            }};
            MethodExecuterVisitor<Integer> visitor = new MethodExecuterVisitorImpl();
            Integer greater = visitor.visitExec(exec);
            list.add(greater);
            times.add(System.currentTimeMillis() - start);
        }

        log.info("Average antlr one initialization: {}", calcAverage(times));
    }

    @Test
    void calcAverageForJava() {
        List<Long> times = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            long start = System.currentTimeMillis();
            Integer greater = test.findGreater();
            times.add(System.currentTimeMillis() - start);
            list.add(greater);
        }


        log.info("Average java: {}", calcAverage(times));
    }

    @Test
    void calcAverageForAntlrWithEveryTimeInitialization() {
        List<Long> times = new ArrayList<>();

        for (int i = 0; i < 1000; i++) {
            long start = System.currentTimeMillis();
            MethodExecuterLexer lexer1 = new MethodExecuterLexer(CharStreams.fromString("test.findGreater()"));
            CommonTokenStream tokens = new CommonTokenStream(lexer1);
            MethodExecuterParser parser1 = new MethodExecuterParser(tokens);
            MethodExecuterParser.ExecContext exec = parser1.exec();

            exec.objects = new HashMap<String, Object>() {{
                put("test", test);
            }};
            MethodExecuterVisitor<Integer> visitor = new MethodExecuterVisitorImpl();
            Integer greater = visitor.visitExec(exec);
            list.add(greater);
            times.add(System.currentTimeMillis() - start);
        }


        log.info("Average antlr everytime initialization: {}", calcAverage(times));
    }

    @Test
    void calcAntlrLexer() {
        List<Long> times = new ArrayList<>();
        StringBuilder cmd = new StringBuilder();
        for (int i = 0; i < 100; i++) {
            cmd.append("test.findGreater()\n");
        }
        long start = System.currentTimeMillis();
        MethodExecuterLexer lexer1 = new MethodExecuterLexer(CharStreams.fromString(cmd.toString()));
        CommonTokenStream tokens = new CommonTokenStream(lexer1);
        MethodExecuterParser parser1 = new MethodExecuterParser(tokens);
        MethodExecuterParser.ExecContext exec = parser1.exec();


        exec.objects = new HashMap<String, Object>() {{
            put("test", test);
        }};
        MethodExecuterVisitor<Integer> visitor = new MethodExecuterVisitorImpl();
        Integer greater = visitor.visitExec(exec);
        list.add(greater);
        times.add(System.currentTimeMillis() - start);

        log.info("Average antlr 100 times command: {}", calcAverage(times));

        log.info("Stamps: {}", test.getTimestamps());

        assertTrue(true, "Should be true");
    }

    @Test
    void calcAverageForAntlrWithOneInitializationFewLexers() {
        List<Long> times = new ArrayList<>();
        MethodExecuterLexer lexer1 = new MethodExecuterLexer(CharStreams.fromString("test.findGreater()"));
        MethodExecuterLexer lexer2 = new MethodExecuterLexer(CharStreams.fromString("test.findLeast()"));
        MethodExecuterLexer lexer3 = new MethodExecuterLexer(CharStreams.fromString("test.rotate()"));
        CommonTokenStream tokens = new CommonTokenStream(lexer1);
        CommonTokenStream tokens2 = new CommonTokenStream(lexer2);
        CommonTokenStream tokens3 = new CommonTokenStream(lexer3);
        MethodExecuterParser parser1 = new MethodExecuterParser(tokens);
        MethodExecuterParser parser2 = new MethodExecuterParser(tokens2);
        MethodExecuterParser parser3 = new MethodExecuterParser(tokens3);
        MethodExecuterParser.ExecContext exec = parser1.exec();
        MethodExecuterParser.ExecContext exec2 = parser2.exec();
        MethodExecuterParser.ExecContext exec3 = parser3.exec();

        for (int i = 0; i < 1000; i++) {
            long start = System.currentTimeMillis();

            exec.objects = new HashMap<String, Object>() {{
                put("test", test);
            }};

            exec2.objects = new HashMap<String, Object>() {{
                put("test", test);
            }};

            exec3.objects = new HashMap<String, Object>() {{
                put("test", test);
            }};
            MethodExecuterVisitor<Integer> visitor = new MethodExecuterVisitorImpl();
            Integer greater = visitor.visitExec(exec);
            Integer least = visitor.visitExec(exec2);
            Integer rotate = visitor.visitExec(exec3);
            list.add(greater);
            list.add(least);
            list.add(rotate);

            times.add(System.currentTimeMillis() - start);
        }

        log.info("Average java 3 methods: {}", calcAverage(times));
    }

    @Test
    void averageForAntlrWithOneInitializationFewLexers() {
        List<Long> times = new ArrayList<>();
        MethodExecuterLexer lexer1 = new MethodExecuterLexer(CharStreams.fromString("test.findGreater()"));
        MethodExecuterLexer lexer2 = new MethodExecuterLexer(CharStreams.fromString("test.findLeast()"));
        MethodExecuterLexer lexer3 = new MethodExecuterLexer(CharStreams.fromString("test.rotate()"));
        CommonTokenStream tokens = new CommonTokenStream(lexer1);
        CommonTokenStream tokens2 = new CommonTokenStream(lexer2);
        CommonTokenStream tokens3 = new CommonTokenStream(lexer3);
        MethodExecuterParser parser1 = new MethodExecuterParser(tokens);
        MethodExecuterParser parser2 = new MethodExecuterParser(tokens2);
        MethodExecuterParser parser3 = new MethodExecuterParser(tokens3);
        MethodExecuterParser.ExecContext exec = parser1.exec();
        MethodExecuterParser.ExecContext exec2 = parser2.exec();
        MethodExecuterParser.ExecContext exec3 = parser3.exec();

        for (int i = 0; i < 1000; i++) {
            long start = System.currentTimeMillis();

            exec.objects = new HashMap<String, Object>() {{
                put("test", test);
            }};

            exec2.objects = new HashMap<String, Object>() {{
                put("test", test);
            }};

            exec3.objects = new HashMap<String, Object>() {{
                put("test", test);
            }};
            MethodExecuterVisitor<Integer> visitor = new MethodExecuterVisitorImpl();
            Integer greater = visitor.visitExec(exec);
            Integer least = visitor.visitExec(exec2);
            Integer rotate = visitor.visitExec(exec3);
            list.add(greater);
            list.add(least);
            list.add(rotate);

            times.add(System.currentTimeMillis() - start);
        }

        log.info("Average antlr one initialization 3 lexers: {}", calcAverage(times));
    }

    public double calcAverage(List<Long> times) {
        Long sum = 0L;
        for (Long time : times) {
            sum += time;
        }
        return (double) sum / (double) times.size();
    }

    @BeforeEach
    void setUp() {
        test.clearStamps();
    }

    @AfterEach
    void tearDown() {

    }

    @BeforeAll
    static void beforeAll() {
        test = new antlr.Test();

        test.generateCollections(1000, 0, Integer.MAX_VALUE);
    }

    private static antlr.Test test;
    private List<Integer> list = new ArrayList<>();
}